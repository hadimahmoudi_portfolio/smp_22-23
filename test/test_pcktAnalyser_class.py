import json, os, sys
from collections import Counter


def loadfile():
    with open("./dataset_voor_test.json", "r") as file:
        datasetJson = json.load(file)
    return datasetJson


# Het pad naar de map 'code' verkrijgen
code_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'analyser'))

# Het pad toevoegen aan het systeempad
sys.path.append(code_path)

from pcktAnalyser_class import pcktAnalyser




""" pcktList is een zelfgemaakte dataset om pcktAnalyser klasse te testen.
Deze dataset bevat 11 netwerk-pakketjes. 

Apparaten: 
    In deze dataset staan 6 verchillende IP-addressen.

    Gebruikers:
    1. A_host, src ip: A_host, src port: 1001
    2. B_host, src ip: B_host, src port: 1002
    3. C_host, src ip: C_host, src port: 1003
    4. D_host, src ip: D_host, src port: 1004

    Servers:
    1. server_01, src ip: server_01, src port: 80
    2. server_2, src ip: server_02, src port: 443

TCP-handshakes:
    In deze dataset staan 2 complete tcp-3ways-handshakes tussen:
    C_host & server_01, A_host & server_01

    Verder staan 2 niet complete tcp-3ways-handshakes tussen:
    B_host & server_02, D_host & server_01.

Protocollen:
    In deze dataset komen 10 keer "tcp" en 1 keer "http" protocollen voor.

"""
pcktList = loadfile()


#Deze functie test "getServer" methode uit pcktAnalyser klasse.
def testGetServer():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" als argumnet.
    analyser = pcktAnalyser(pcktList)

    #Voor deze test worden de volgende poortnummers overwogen.\
    #Elk ander poort nummer kan worden vervangen.
    dstPorts = ["443", "80"]

    #roep en bewaar "getServer" methode uit pcktAnalyser klasse.
    servers = analyser.getServer(dstPorts)

    #Het gewenste resultaat o.b.v. de pcktList informatie.\
    #Een list met IP-adressen van alle servers.\
    #In dit geval zijn er 2 servers.
    expected_result = ["server_01", "server_02"]

#Deze functie test "getUser" methode uit pcktAnalyser klasse.
def testGetUser():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" als argumnet.
    analyser = pcktAnalyser(pcktList)

    #Voor deze test worden de volgende poortnummers overwogen.\
    #Elk ander poort nummer kan worden vervangen.
    dstPorts = ["443", "80"]

    #roep en bewaar "getServer" methode uit pcktAnalyser klasse.
    servers = analyser.getServer(dstPorts)

    #roep en bewaar "getUser" methode uit pcktAnalyser klasse.\
    #geeft bovenstaande "servers" als argumnet.
    users = analyser.getUser(servers)

    #Het gewenste resultaat op basis van de pcktList informatie.\
    #Een dictionery met IP-adressen van alle gebruiker als sleutel.\
    #en het aantal keren dat elk als src ip in de dataset is geweest als waarde.
    #In dit geval zijn er 4 gebruikers.
    expected_result = {'A_host': 3, 'C_host': 2, 'B_host': 1, 'D_host': 1}

    #test of het uitput van "getUser" methode gelijk met "expectedResult" is.
    assert users  == expected_result    


#Deze functie test "getProtocols" methode uit pcktAnalyser klasse.
def testGetProtocols():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" als argumnet.
    analyser = pcktAnalyser(pcktList)
    
    #roep en bewaar "getProtocols" methode uit pcktAnalyser klasse.
    protocols = analyser.getProtocols()

    #Het gewenste resultaat op basis van de pcktList informatie.\
    #In dit geval één "tcp" en één "http".
    expected_result = Counter({'eth:ethertype:ip:tcp': 10, 'eth:ethertype:ip:tcp:http': 1})

    #test of het uitput van "getProtocols" methode gelijk met "expectedResult" is.
    assert protocols == expected_result


#Deze functie test "getType" methode uit pcktAnalyser klasse.
def testGetType():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" uit __init__ method als argumnet.
    analyser = pcktAnalyser(pcktList)

    #roep en bewaar "getType" methode uit klasse.
    tcpType = analyser.getType()

    #Het gewenste resultaat op basis van de pcktList informatie.\
    #Een dictionery met 10 netwerk-pakketjes met hun informatie.
    expectedResult = {
        "Index in dataset: 1": {"time": '01-05-1970 03:24:00.965305',"TYPE": "SYN","src ip": "C_host",
                                "src port": "1003","dst ip": "server_01","dst port": "80"},
        "Index in dataset: 2": {"time": '01-05-1970 03:25:00.965305',"TYPE": "SYN-ACK","src ip": "server_01",
                                "src port": "80","dst ip": "C_host","dst port": "1003"},
        "Index in dataset: 3": {"time": '01-05-1970 03:26:00.965305',"TYPE": "ACK","src ip": "C_host",
                                "src port": "1003","dst ip": "server_01","dst port": "80"},
        "Index in dataset: 4": {'time': '01-05-1970 03:27:00.965305',"TYPE": "SYN","src ip": "A_host",
                                "src port": "1001","dst ip": "server_01","dst port": "80"},
        "Index in dataset: 5": {'time': '01-05-1970 03:28:00.965305',"TYPE": "SYN" ,"src ip": "B_host",
                                "src port": "1002","dst ip": "server_02","dst port": "443"},
        "Index in dataset: 6": {
            'time': '01-05-1970 03:29:00.965305',
            "TYPE": "SYN-ACK",
            "src ip": "server_01",
            "src port": "80",
            "dst ip": "A_host",
            "dst port": "1001"
        },
        "Index in dataset: 7": {
            'time': '01-05-1970 03:30:00.965305',
            "TYPE": "ACK",
            "src ip": "A_host",
            "src port": "1001",
            "dst ip": "server_01",
            "dst port": "80"
        },
        "Index in dataset: 8": {
            'time': '01-05-1970 03:31:00.965305',
            "TYPE": "SYN-ACK",
            "src ip": "server_02",
            "src port": "443",
            "dst ip": "B_host",
            "dst port": "1002"
        },
        "Index in dataset: 10": {
            'time': '01-05-1970 03:33:00.965305',
            "TYPE": "SYN-ACK",
            "src ip": "server_02",
            "src port": "443",
            "dst ip": "B_host",
            "dst port": "1002"
        },
        "Index in dataset: 11": {
            "time": '01-05-1970 03:34:00.965305',
            "TYPE": "SYN",
            "src ip": "D_host",
            "src port": "1004",
            "dst ip": "server_01",
            "dst port": "80"
        }
        }

    #test of het uitput van "getType" methode gelijk met "expectedResult" is.
    assert tcpType == expectedResult

#Deze functie test "AnalyzeHandshakes" methode uit pcktAnalyser klasse.
def testAnalyzeHandshakes():
        
    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" uit __init__ method als argumnet.
    analyser = pcktAnalyser(pcktList)

    #roep en bewaar "getType" methode uit klasse.
    tcpType = analyser.getType()

    #roep "analyzeHandshakes" methode en bewaar het in handshakes variable.\
    #geeft bovenstaande "tcpType" als argumnet.
    handshakes = analyser.analyzeHandshakes(tcpType)

    #Het gewenste resultaat o.b.v. de pcktList informatie.\
    # Een list met 4 items. 2 compelete handshakes tussen C_host & server_01, A_host & server_01\
    # 2 niet compelete handshakes tussen B_host & server_02, D_host & server_01
    expectedResult = [[['---> TCP handshake is compleet. (src: C_host:1003, dst: server_01:80)',
            '- Eerste pakket: SYN, Index in dataset: 1, time: 01-05-1970 03:24:00.965305 ---> van C_host:1003 naar server_01:80',
            '- Tweede pakket: ACK, Index in dataset: 3, time: 01-05-1970 03:26:00.965305 ---> van C_host:1003 naar server_01:80']],
            
            [['---> TCP handshake is compleet. (src: A_host:1001, dst: server_01:80)',
            '- Eerste pakket: SYN, Index in dataset: 4, time: 01-05-1970 03:27:00.965305 ---> van A_host:1001 naar server_01:80',
            '- Tweede pakket: ACK, Index in dataset: 7, time: 01-05-1970 03:30:00.965305 ---> van A_host:1001 naar server_01:80']],
            
            [['---> TCP-handshake is NIET compleet! (src: B_host:1002, dst: server_02:443)',
                '- Eerste pakket: SYN, Index in dataset: 5, time: 01-05-1970 03:28:00.965305 ---> van B_host:1002 naar server_02:443',
                '- Tweede pakket: niet gevonden!']],
                
                [['---> TCP-handshake is NIET compleet! (src: D_host:1004, dst: server_01:80)',
                '- Eerste pakket: SYN, Index in dataset: 11, time: 01-05-1970 03:34:00.965305 ---> van D_host:1004 naar server_01:80',
                '- Tweede pakket: niet gevonden!']]]

    #test of het uitput van "analyzeHandshakes" methode gelijk met "expectedResult" is.
    assert handshakes == expectedResult


#Deze functie test "FindCompleteHandshakes" methode uit pcktAnalyser klasse.
def testFindCompleteHandshakes():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" uit __init__ method als argumnet.
    analyser = pcktAnalyser(pcktList)

    #roep en bewaar "getType" methode uit klasse.
    tcpType = analyser.getType()

    #roep "findCompleteHandshakes" methode en bewaar het in completeHandshakes variable.\
    #geeft bovenstaande "tcpType" als argumnet.
    completeHandshakes = analyser.findCompleteHandshakes(tcpType)


    #Het gewenste resultaat o.b.v. de pcktList informatie.\
    # Een list met alle complate handsahkes.
    # Er zijn 2 compelete handshakes tussen C_host & server_01, A_host & server_01\
    expectedResult = [[['---> TCP handshake is compleet. (src: C_host:1003, dst: server_01:80)',
            '- Eerste pakket: SYN, Index in dataset: 1, time: 01-05-1970 03:24:00.965305 ---> van C_host:1003 naar server_01:80',
            '- Tweede pakket: ACK, Index in dataset: 3, time: 01-05-1970 03:26:00.965305 ---> van C_host:1003 naar server_01:80']],
            
            [['---> TCP handshake is compleet. (src: A_host:1001, dst: server_01:80)',
            '- Eerste pakket: SYN, Index in dataset: 4, time: 01-05-1970 03:27:00.965305 ---> van A_host:1001 naar server_01:80',
            '- Tweede pakket: ACK, Index in dataset: 7, time: 01-05-1970 03:30:00.965305 ---> van A_host:1001 naar server_01:80']]]
    
    #test of het uitput van "completeHandshakes" methode gelijk met "expectedResult" is.
    assert completeHandshakes == expectedResult


#Deze functie test "FindInCompleteHandshakes" methode uit pcktAnalyser klasse.
def testFindInCompleteHandshakes():

    #roep en bewaar "pcktAnalyser" klasse in analyser variable.\
    #geeft "pcktList" uit __init__ method als argumnet.
    analyser = pcktAnalyser(pcktList)

    #roep en bewaar "getType" methode uit klasse.
    tcpType = analyser.getType()

    #roep "findInCompleteHandshakes" methode en bewaar het in completeHandshakes variable.\
    #geeft bovenstaande "tcpType" als argumnet.
    inCompleteHandshakes = analyser.findInCompleteHandshakes(tcpType)


    #Het gewenste resultaat o.b.v. de pcktList informatie.\
    # Een list met alle niet complete handshakes.
    # Er zijn 2 niet compelete handshakes tussen B_host & server_02, D_host & server_01
    expectedResult = [[['---> TCP-handshake is NIET compleet! (src: B_host:1002, dst: server_02:443)',
                '- Eerste pakket: SYN, Index in dataset: 5, time: 01-05-1970 03:28:00.965305 ---> van B_host:1002 naar server_02:443',
                '- Tweede pakket: niet gevonden!']],
                
                [['---> TCP-handshake is NIET compleet! (src: D_host:1004, dst: server_01:80)',
                '- Eerste pakket: SYN, Index in dataset: 11, time: 01-05-1970 03:34:00.965305 ---> van D_host:1004 naar server_01:80',
                '- Tweede pakket: niet gevonden!']]]
    
    #test of het uitput van "inCompleteHandshakes" methode gelijk met "expectedResult" is.
    assert inCompleteHandshakes == expectedResult


