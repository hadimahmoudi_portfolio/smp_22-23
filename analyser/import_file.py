import json


def readJSON(flnm):
    with open(flnm, 'r') as f:
        data = json.load(f)
    return data
