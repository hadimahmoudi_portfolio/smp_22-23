# Importeer vereiste bibliotheken
from collections import Counter
from datetime import datetime


class pcktAnalyser():

    """ pcktAnalyser klasse t.b.v. Analyze van captured pakketjes.

    Deze klasse geeft enkele standard analyse mogelijkheden om,
    een packetcapture te filteren en controleren op aanwezigheld van aanvals-kenserken.
    Deze klasse werkt met een lijst van pakket-info.
    Deze packetinfo is gestructureerd en gemaakt m.b.v. de Wireshark "EXPORT TO JSON" functionalitelt.
    """
    
    def __init__(self, pcktList):
        """ Initialiseer een analyser object.

        Argument:
        De methode __init__ krijgt een gestructureerde lijst van netwerk pakketjes.
        """
        self.pcktList = pcktList

    def getServer(self, dstPorts):
        """ Specificeert de servers apparaten o.b.v. bestemming poorten (dst ports).

        Argument:
        1. De methode getServer krijgt een of meerdere dst port nummer als een instance variabelen.

        Description:
        De methode getServer loopt de geïmporteerde dataset uit de __init__ methode door 
        en kijkt naar het dst port van elk pakketje. 
         
        Return: een lijst met IP-adressen van servers.
        """


        # De methode getServers krijgt een of meerder poort nummers.
        self.dstPorts = dstPorts
    

        # Een lijst om resultaat van getServers methode bijhouden.
        servers = []    

        #loopt elk pakketje(p) in dataset door.
        for p in self.pcktList:

            # Selecteer het waarde van "ip.dst" uit "ip" laag en bewaar het in een variable.
            dstIp = p["_source"]["layers"]["ip"]["ip.dst"]

            # Selecteer het waarde van "tcp.dstport" uit "tcp" laag en bewaar het in een variable.
            dstPort = p["_source"]["layers"]["tcp"]["tcp.dstport"]

            # Als de geselecteerde poorten voorkomen in de initiële lijst van poorten\
            # programa gaat verder.
            if dstPort in dstPorts:

                #Als het gevonden IP-adres niet in de servers staat.
                if dstIp not in servers:

                    # Dit IP-adres wordt toegevoegd aan de servers.
                    servers.append(dstIp)

        #terug krijgen van gemaakte lijst.
        return servers

    def getUser(self, servers):

        """ Specificeert de gebruikers apparaten o.b.v. bron ip-adressen(src ips).

        Argument:
        1. De methode getUser gebruikt het output van getServer methode als een instance variabelen.

        Description:
        De methode getUser loopt de geïmporteerde dataset uit de __init__ methode door\
        en kijkt naar het src ip van elk pakketje. 

        De gevonden src IP-adressen vergeleken met de lijst van servers uit getServer methode.\
        De adressen die in de lijst van servers staan, zullen niet worden overwogen.\
        IP-adressen die overblijven, beschouwd als gebruiker.
         
        Return: een dictionary met IP-adressen van gebruikers als sleutel\
            en het aantal keren dat elk als src ip in de dataset is geweest als waarde.
        """


        #Een dictionary om resultaat van getUser methode bijhouden.
        users = {}

        #loopt elk pakket(p) in dataset door.
        for p in self.pcktList:

            # Selecteer het waarde van "ip.src" uit "ip" laag en bewaar het in een variable.
            srcIp = p["_source"]["layers"]["ip"]["ip.src"]

            # Als de geselecteerde IP-adressen niet staan in servers\
            # programa gaat verder.
            if srcIp not in servers:

                #Als het gevonden src IP-adres al in de users staat.\
                #Telt het waarde van hem één op. 
                if srcIp in users:
                    users[srcIp] += 1

                # Als het gevonden src IP-adres niet in de users staat.\
                # Dit IP-adres wordt als een nieuwe sleutel aan de dictionary toegevoegd.           
                else:
                    users[srcIp] = 1

        # Het doel van deze funcite is om meest actieve gebruikers te identificeren. 
        # Daarvoor is het dictionary van users gesorteerd op het waarde (aantal pakketten 
        # van elk sleutel (IP-adres) van hoog naar laag.
        sortedUsers = sorted(users.items(), key=lambda x:x[1], reverse=True)

        # Als de sorted-methode wordt gebruikt, wordt het dictionary geconverteerd naar een lijst.\
        # De lijst verandert weer in een dictionary.
        sortedUsersDic = dict(sortedUsers)

        #terug krijgen van gemaakte dictionary.
        return sortedUsersDic

    def getProtocols(self):
        """Specificeert het type communicatieprotocollen en hun aantal o.b.v. "frame.protocols" veld in de frame-laag.
        
        Description:
        De methode getProtocols loopt de geïmporteerde dataset uit de __init__ methode door\
        en kijkt naar het "frame.protocols" veld van elk pakketje. 

        Return: een lijst met de naam van elk protocol en het aantal van elk.
        """
        
        # Een lijst om resultaat van getProtocols methode bijhouden.
        usedProtocols = []

        # loopt elk pakketje in dataset door.
        for item in self.pcktList:

            # Selecteer "frame.protocols" veld en bewaar het in een variable.
            frameProtocols = item["_source"]["layers"]["frame"]["frame.protocols"]

            # Voegt het protocol toe aan de lijst.
            usedProtocols.append(frameProtocols)

        # Met behulp van de Counter methode telt het elk uniek protocol en telt het aantal.
        # Slaat het resultaat op in een nieuwe lijst.
        protocolCount = Counter(usedProtocols)

        #terug krijgen van gemaakte lijst.
        return protocolCount

    def getType(self):
        """ Specificeert het type van pakketjes o.b.v. tcp vlaggen.

        Deze methode loopt de geïmporteerde dataset uit de __init__ methode door
        en kijkt naar de waarde van vlag van elk pakketje in de tcp laag
        en o.b.v. deze waarde specificeert het type van elke pakketjes.

        Het doel hier is om de pakketten te identificeren die deel uitmaken van TCP-3ways handshakes. In dit geval zijn er per vlag drie mogelijkheden:
        1. Als de waarde van vlag "0x0002" is, betekent dit dat deze een SYN pakketje is.
        2. Als de waarde van vlag "0x0012" is, betekent dit dat deze een SYN-ACK pakketje is.
        3. Als de waarde van vlag "0x0010" is, betekent dit dat deze een ACK pakketje is.

         
        Return: een gestructureerde dictionary.
        waarin de sleutel van elk pakketje is zijn index in de originele lijst (dataset).
        De waarde van elk pakketje bestaat uit het type (SYN, SYN-ACK, ACK), verzonden tijd,
        src ip, src port, dst ip en dst port.
        """

        # Een dictionary om resultaat van getType methode bijhouden.
        tcpType = {}

        # Specificeer de index van elk pakket
        packetId = 0

        # loopt elk pakketje in dataset door.
        for item in self.pcktList:

            # voor elk pakket telt een op.
            packetId += 1

            # Kijkt aan "frame" laag in dataset.
            if "frame" in item["_source"]["layers"]:

                # Selecteer "frame.time" veld en bewaar het in een variable.
                frameTime = item["_source"]["layers"]["frame"]["frame.time"]

                #6. In de "frame.time" veld staat tijd in deze vorm: "Jan  5, 1970 03:48:31.873978000 W. Europe Standard Time"
                #Onderstaande stappen, schoon het waarde van "frame.time" en maakt het gemakkelijker om eraan te blijven werken.
                #Dit is gewenste tijd vorm: "01-05-1970 03:24:22.499972"
                frameTimeCleaned = frameTime.split(' W.')[0].strip()
                microseconds = frameTimeCleaned[-9:]
                frameTimeCleaned = frameTimeCleaned[:-9] + microseconds[:6]
                dtObject = datetime.strptime(frameTimeCleaned, '%b %d, %Y %H:%M:%S.%f')
                time = dtObject.strftime('%m-%d-%Y %H:%M:%S.%f')

            #8. Kijkt naar "tcp" laag in dataset.
            if "tcp" in item["_source"]["layers"]:

                #Bewaar het waarde van "tcp.srcport" veld in "srcPort" variable.
                srcPort = item["_source"]["layers"]["tcp"]["tcp.srcport"]
                
                #Bewaar het waarde van "tcp.dstport" veld in "dstPort" variable.
                dstPort = item["_source"]["layers"]["tcp"]["tcp.dstport"]

                #Bewaar het waarde van "tcp.flags" veld in "tcpFlags" variable.
                tcpFlags = item["_source"]["layers"]["tcp"]["tcp.flags"]

            # Kijkt naar "ip" laag in dataset.
            if "ip" in item["_source"]["layers"]:

                #Bewaar het waarde van "ip.src" veld in "srcIp" variable.
                srcIp = item["_source"]["layers"]["ip"]["ip.src"]

                #Bewaar het waarde van "ip.dst" veld in "dstIp" variable.
                dstIp = item["_source"]["layers"]["ip"]["ip.dst"]

                #Standaardwaarden voor vlaggen van TCP 3 ways handshakes.
                # syn.flag == "0x0002"
                # synAck.flag == "0x0012"   
                # ack.flag == "0x0010"
                tcp3wayFlags = ["0x0002", "0x0012", "0x0010"]

                # Specificeert het type van pakketjes o.b.v. tcp vlaggen.
                if tcpFlags in tcp3wayFlags:

                    if tcpFlags == "0x0002":
                        tcpFlags = "SYN"
                    
                    elif tcpFlags == "0x0012":
                        tcpFlags = "SYN-ACK"
                    
                    elif tcpFlags == "0x0010":
                        tcpFlags = "ACK"
                    
                    # Alle variabelen die in de bovenstaande stappen zijn gedefinieerd \
                    # worden verzameld en toegevoegd in eerder gemaakte "tcpType" dictionary.\
                    tcpType[f"Index in dataset: {packetId}"] = {
                        "time": time,
                        "TYPE": tcpFlags,
                        # "TCP-stream": tcp_stream,
                        "src ip": srcIp,
                        "src port": srcPort,
                        "dst ip": dstIp,
                        "dst port": dstPort
                }
        
        #terug krijgen van gemaakte dictionary.
        return tcpType
    
    def analyzeHandshakes(self, tcpType):
        """O.b.v. de verzonden SYN en ACK berichten bepaalt het 
        of de tcp 3ways handshakes tot stand is gebracht of niet.
        
        Deze analyzeHandshakes methode krijgt de output van getType methode
        als een instance variabelen.

        Dit methode loopt de gemaakte dictionary uit de getType-methode door
        en zoekt voor het eerste SYN pakketje en bepaalt de src ip, src port,
        dst ip en dst port van hem. Verder bewaar ze in de variabelen.

        Het zoeken gaat verder om het volgende pakket te vinden dat exact
        de source-en destination specificaties van het SYN pakket heeft.
        Als het type van dit pakket "ACK" is, wordt geconcludeerd dat
        de handshake succesvol is afgerond. Als het pakkettype iets anders is,
        wordt geconcludeerd dat de gebruiker de handshake niet heeft voltooid.

        Return: een list met informatie over elk tcp-3ways-handshake.
        """
        
        #Een list om resultaat van analyzeHandshakes methode bijhouden.
        handshakes = []

        #loopt sleutel(k) en waarde(p) in dictionary(getType) door.
        for k, p in tcpType.items():

            #Zoekt voor pakktje met SYN type.\
            #Als het het vindt, bewaar het zijn informatie in variabelen.
            if p.get('TYPE') == 'SYN':
                synKey = k
                synPakket = p
                synTime = synPakket.get("time")
                synSrcIp = synPakket.get('src ip')
                synSrcPort = synPakket.get('src port')
                synDstIp = synPakket.get('dst ip')
                synDstPort = synPakket.get('dst port')


                # Zoek volgend pakket in de dataset\
                # met exact de src-en dst specificaties van het SYN pakket.\
                # Als het het vindt, bewaar het zijn informatie in variabelen.
                nextPacketKey = next((k for k, p in tcpType.items() if p.get('src ip') == synSrcIp and
                                                                        p.get('src port') == synSrcPort and
                                                                        p.get('dst ip') == synDstIp and
                                                                        p.get('dst port') == synDstPort and
                                                                        p.get("time") > synTime and
                                                                        k != synKey),
                                        None)

                
                if nextPacketKey:

                    #Specificeert het type van tweede pakketje en bewaar het in een variable.
                    nextPacketType = tcpType[nextPacketKey].get('TYPE')

                    #Specificeert het verzond tijd van tweede pakketje en bewaar het in een variable.
                    nextPacketTime = tcpType[nextPacketKey].get("time")

                    #Als het type van het pakket iets anders dan "ACK" is, \
                    #Dit bericht is gelogd: Geen complete TCP handshake.
                    if nextPacketType != "ACK":
                        message = "!!!! Geen complete TCP handshake. !!!!"
                        
                    #In andere geval, het type van pakket is "ACK"\
                    #Dus een compelete handshake.
                    else:
                        message = "---> TCP handshake is compleet."


                    # Verder worden alle variabelen die in de bovenstaande stappen zijn gedefinieerd \
                    # van eerste pakket (SYN) en tweede pakket (eventueel ACK) verzameld in een "result" variable.\
                    # Deze variable heeft drie items.\
                    # Eerste item: gegenereerd bericht, src ip:src port, dst ip:dst port. \
                    # Tweede item: type van eerste pakket(SYN), index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.
                    # Derde item: type van tweede pakket, index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.

                    result = [[(f"{message} (src: {synSrcIp}:{synSrcPort}, dst: {synDstIp}:{synDstPort})"),
                                   (f"- Eerste pakket: SYN, {synKey}, time: {synTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}"),
                                   (f"- Tweede pakket: {nextPacketType}, {nextPacketKey}, time: {nextPacketTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}")]]
                    
                    # Result wordt toegevoegd aan de "handshakes" lijst die al is gemaakt.
                    handshakes.append(result)
                
                #Als er geen pakket bestaan met exact de src-en dst specificaties van het SYN\
                #dan wordt het item 3 in variable result veranderen.\
                # Inplaats van gegevens van tweede pakket, komt een boodschap: Tweede pakket: niet gevonden!.
                else:
                    result = [[(f"---> TCP-handshake is NIET compleet! (src: {synSrcIp}:{synSrcPort}, dst: {synDstIp}:{synDstPort})"),
                               (f"- Eerste pakket: SYN, {synKey}, time: {synTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}"),
                               ("- Tweede pakket: niet gevonden!")]]
                    
                    # Result wordt toegevoegd aan de "handshakes" lijst die al is gemaakt.
                    handshakes.append(result)
               
        #terug krijgen van gemaakte list.
        return handshakes

    def findCompleteHandshakes(self, tcpPckts):

        """O.b.v. de verzonden SYN en ACK berichten maakt
        een list van complete tcp-3ways-handshakes.

        Deze methode is eigenlijk een onderdeel van analyzeHandshakes methode.
        De meeste commentaren zijn dus hetzelfde.
        
        Deze findCompleteHandshakes methode krijgt de output van getType methode
        als een instance variabelen.

        Dit methode loopt de gemaakte dictionary uit de getType-methode door
        en zoekt voor het eerste SYN pakketje en bepaalt de src ip, src port,
        dst ip en dst port van hem. Verder bewaar ze in de variabelen.

        Het zoeken gaat verder om het volgende pakket te vinden
        dat exact de source-en destination specificaties van het SYN pakket heeft.
        Als de type van deze pakket "ACK" is, Geconcludeerd wordt dat de handshake succesvol is afgerond.

        Return: een list met alle afgeronde handshakes.
        """

        #Een list om resultaat van findCompleteHandshakes methode bijhouden.
        completeHandshakes = []

        #loopt sleutel(k) en waarde(p) in dictionary(getType) door.        
        for k, p in tcpPckts.items():

            #Zoekt voor pakktje met SYN type.\
            #Als het het vindt, bewaar het zijn informatie in variabelen.
            if p.get('TYPE') == 'SYN':
                synKey = k
                synPakket = p
                synTime = synPakket.get("time")
                synSrcIp = synPakket.get('src ip')
                synSrcPort = synPakket.get('src port')
                synDstIp = synPakket.get('dst ip')
                synDstPort = synPakket.get('dst port')


                # Zoek volgend pakket in de dataset\
                # met exact de src-en dst specificaties van het SYN pakket.\
                # Als het het vindt, bewaar het zijn informatie in variabelen.
                nextPacketKey = next((k for k, p in tcpPckts.items() if p.get('src ip') == synSrcIp and
                                                                        p.get('src port') == synSrcPort and
                                                                        p.get('dst ip') == synDstIp and
                                                                        p.get('dst port') == synDstPort and
                                                                        p.get("time") > synTime and
                                                                        k != synKey),
                                        None)

                
                if nextPacketKey:
                    
                    #Specificeert het type van tweede pakketje en bewaar het in een variable.
                    nextPacketType = tcpPckts[nextPacketKey].get('TYPE')

                    #Specificeert het verzond tijd van tweede pakketje en bewaar het in een variable.
                    nextPacketTime = tcpPckts[nextPacketKey].get("time")

                    #Als het type van het pakket "ACK" is, \
                    #Dit bericht is gelogd: TCP handshake is compleet.
                    if tcpPckts[nextPacketKey].get('TYPE') == "ACK":
        
                        message = "---> TCP handshake is compleet."

                    # Verder worden alle variabelen die in de bovenstaande stappen zijn gedefinieerd \
                    # van eerste pakket (SYN) en tweede pakket (eventueel ACK) verzameld in een "result" variable.\
                    # Deze variable heeft drie items.\
                    # Eerste item: gegenereerd bericht, src ip:src port, dst ip:dst port. \
                    # Tweede item: type van eerste pakket(SYN), index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.
                    # Derde item: type van tweede pakket, index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.

                        result = [[(f"{message} (src: {synSrcIp}:{synSrcPort}, dst: {synDstIp}:{synDstPort})"),
                                   (f"- Eerste pakket: SYN, {synKey}, time: {synTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}"),
                                   (f"- Tweede pakket: {nextPacketType}, {nextPacketKey}, time: {nextPacketTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}")]]
                        
                        # Result wordt toegevoegd aan de "completeHandshakes" lijst die al is gemaakt.
                        completeHandshakes.append(result)
               
        #terug krijgen van gemaakte list.
        return completeHandshakes
    
    def findInCompleteHandshakes(self, tcpPckts):

        """O.b.v. de verzonden SYN en ACK berichten maakt
        een list van niet complete tcp-3ways-handshakes.

        De Methode findInCompleteHandshakes is eigenkijk een onderdeel van methode analyzeHandshakes.
        De meeste commentaars zijn dus hetzelfde.
        
        De Methode findInCompleteHandshakes krijgt een gestructureerde dictionary van netwerk-pakketjes.

        Dit methode loopt de gemaakte dictionary uit de getType-methode door
        en zoekt voor het eerste SYN pakketje en bepaalt de src ip, src port,
        dst ip en dst port van hem. Verder bewaar ze in de variabelen.

        Het zoeken gaat verder om het volgende pakket te vinden
        dat exact de source-en destination specificaties van het SYN pakket heeft.
        Als de type van deze pakket niet "ACK" is, Geconcludeerd wordt dat de handshake is niet afgerond.

        Return: een list met alle niet afgeronde handshakes.
        """

        #Een list om resultaat van findInCompleteHandshakes methode bijhouden.
        inCompleteHandshakes = []


        #loopt sleutel(k) en waarde(p) in dictionary(getType) door. 
        for k, p in tcpPckts.items():

            #Zoekt voor pakktje met SYN type.\
            #Als het het vindt, bewaar het zijn informatie in variabelen.
            if p.get('TYPE') == 'SYN':
                synKey = k
                synPakket = p
                synTime = synPakket.get("time")
                synSrcIp = synPakket.get('src ip')
                synSrcPort = synPakket.get('src port')
                synDstIp = synPakket.get('dst ip')
                synDstPort = synPakket.get('dst port')

                # Zoek volgend pakket in de dataset\
                # met exact de src-en dst specificaties van het SYN pakket.\
                # Als het het vindt, bewaar het zijn informatie in variabelen.
                nextPacketKey = next((k for k, p in tcpPckts.items() if p.get('src ip') == synSrcIp and
                                                                        p.get('src port') == synSrcPort and
                                                                        p.get('dst ip') == synDstIp and
                                                                        p.get('dst port') == synDstPort and
                                                                        p.get("time") > synTime and
                                                                        k != synKey),
                                        None)

                
                if nextPacketKey:

                    #Specificeert het type van tweede pakketje en bewaar het in een variable.
                    nextPacketType = tcpPckts[nextPacketKey].get('TYPE')

                    #Specificeert het verzond tijd van tweede pakketje en bewaar het in een variable.
                    nextPacketTime = tcpPckts[nextPacketKey].get("time")

                    #Als het type van het pakket niet "ACK" is, \
                    #Dit bericht is gelogd: TCP-handshake is NIET compleet!.
                    if nextPacketType != "ACK":
        
                        message = "---> TCP-handshake is NIET compleet!"
                        
                    # Verder worden alle variabelen die in de bovenstaande stappen zijn gedefinieerd \
                    # van eerste pakket (SYN) en tweede pakket (SYN of SYN-ACK) verzameld in een "result" variable.\
                    # Deze variable heeft drie items.\
                    # Eerste item: gegenereerd bericht, src ip:src port, dst ip:dst port. \
                    # Tweede item: type van eerste pakket(SYN), index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.
                    # Derde item: type van tweede pakket, index van originele dataset, verzond tijd, src ip:src port, dst ip:dst port.
                    
                        result = [[(f"{message} (src: {synSrcIp}:{synSrcPort}, dst: {synDstIp}:{synDstPort})"),
                                   (f"- Eerste pakket: SYN, {synKey}, time: {synTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}"),
                                   (f"- Tweede pakket: {nextPacketType}, {nextPacketKey}, time: {nextPacketTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}")]]
                    
                        # Result wordt toegevoegd aan de "inCompleteHandshakes" lijst die al is gemaakt.
                        inCompleteHandshakes.append(result)

                #Als er geen pakket bestaan met exact de src-en dst specificaties van het SYN\
                #dan wordt het item 3 in variable result veranderen.\
                # Inplaats van gegevens van tweede pakket, komt een boodschap: Tweede pakket: niet gevonden!.
                else:
                    result = [[(f"---> TCP-handshake is NIET compleet! (src: {synSrcIp}:{synSrcPort}, dst: {synDstIp}:{synDstPort})"),
                               (f"- Eerste pakket: SYN, {synKey}, time: {synTime} ---> van {synSrcIp}:{synSrcPort} naar {synDstIp}:{synDstPort}"),
                               ("- Tweede pakket: niet gevonden!")]]
                    
                    # Result wordt toegevoegd aan de "inCompleteHandshakes" lijst die al is gemaakt.
                    inCompleteHandshakes.append(result)

        #terug krijgen van gemaakte list.
        return inCompleteHandshakes
    
