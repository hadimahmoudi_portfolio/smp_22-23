#! /usr/bin/env python3
# Importeer vereiste bibliotheken
import argparse
import import_file
import pcktAnalyser_class as p

#Dit functie is verantwoordelijk voor het koppelen van de methoden van pcktAnalyser klasse aan de command line.
#Het geeft een key aan elke methode, zodat door die key in de command line in te voeren, de bijbehorende methode wordt uitgevoerd.
def getArgs():
    parser = argparse.ArgumentParser(
        description='analyseer een packetcapture file in JSON format.')
    parser.add_argument('pcktList',
                        help="het source JSON bestand,\
                            met relatief path en optioneel wildcards")
    
    parser.add_argument("-a", "--apparaten", action="store_true", help="Identificeert servers en gebruikers.\
                        -------------------------------------> ./file.py file.json -a")
    
    parser.add_argument("-p", "--protocols", action="store_true", help="toont gebruikte protocols.\
                        -----------------------------------------------> ./file.py file.json -p")

    parser.add_argument("-t", "--tcpType", action="store_true", help="toont analyse informatie van pakketten m.b.t. TCP-3way handshakes. \
                        -------> ./file.py file.json -t")
    
    parser.add_argument("-H", "--handshakes", action="store_true", help="toont het status van elke TCP-handshake.\
                        ---------------------------------> ./file.py file.json -H")
    
    parser.add_argument("-c", "--completeHandshakes", action="store_true", help="toont alle voltooide TCP-handshakes.\
                        -------------------------------------> ./file.py file.json -c")
    
    parser.add_argument("-i", "--inCompleteHandshakes", action="store_true", help="toont alle niet onvoltooide TCP-handshakes.\
                        ------------------------------> ./file.py file.json -i")
    
    args = parser.parse_args()
    return args



if __name__ == "__main__":

    #oproepen van getArgs functie
    args = getArgs()

    #open dataset
    loadData = import_file.readJSON(args.pcktList)
    readyDataset = p.pcktAnalyser(loadData)


    # Als één van de argumenten op de command line wordt ingevoerd.\
    # Druk de relevante resultaten af.
    if args.pcktList:

        # Als argument -a/ --apparaten wordt uitgevoerd.
        if args.apparaten:
            
            # De dstPorts variabele wordt gedefinieerd als invoer van de gebruiker.
            # Het accepteert alleen cijfers als invoer.
            while True:
                dstPorts = input("Voer de bestemmings poorten in waarmee u de servers wilt identificeren: ")
                dstPorts = [num.strip() for num in dstPorts.split(",")]
    
                if all(num.isdigit() for num in dstPorts):
                    break
                else:
                    print("Ongeldige invoer. Voer alstublieft slechts nummers, (gescheiden door komma's).")
                    print()
            
            # oproep getServer methode uit pcktAnalyser klasse.
            servers = readyDataset.getServer(dstPorts)

            # Als de lijst met servers niet leeg is. De lijst wordt afgedrukt met het volgende bericht.
            if len(servers) > 0:
                print(f"- IP-adressen met poortnummer {dstPorts} wordt beschouwd als een server.")
                print(f"- Aantal gevonden servers: {len(servers)}")
                print(f"lijst van servers:")
                print(servers)

            # Als de lijst leeg is (geen server gevonden)
            if len(servers) == 0:
                print(f"- Er is geen server apparaten met bestemming poort {dstPorts}.")

            # Oproep getUser methode uit pcktAnalyser klasse.
            users = readyDataset.getUser(servers)

            # Extra informatie voor gebruiker.
            print()
            print(f"- IP-adressen die hun poortnummer niet {dstPorts} is, wordt beschouwd als een gebruiker.")

            # De lengte van de users dictionary (die het aantal gebruikers weergeeft) wordt afgedrukt.
            print(f"- Aantal gevonden gebruikers: {len(users)}")

            # De gebruiker wordt gevraagd hoeveel items van de users dictionary hij wil zien?
            # Het accepteert alleen cijfers als invoer.
            while True:
                try:
                    userId = int(input("Van hoog naar laag, hoeveel van de meest actieve gebruikers wilt u zien? "))
                    break  
                
                # Het accepteert alleen cijfers als invoer.
                except ValueError:
                    print("Ongeldige invoer. Voer alstublieft een geheel getal in.")
                    print()

            # met behulp van de gebruikersinvoer die is opgeslagen in de userId variabele. \
            # De gebruiker kan het gewenste aantal items uit de users dictionary bekijken.
            mostActiveUsers = list(users.items())[:userId]

            # Afdruk van het resultaat.
            print(f"\nDe {userId} meeste actieve gebruikers:")
            for key, value in mostActiveUsers:
                print(f"---> {key}, Aantal pakketten naar servers:{value}")
            print()

        # Als argument -p/ --protocols wordt uitgevoerd.
        if args.protocols:

            #oproep getProtocols methode uit pcktAnalyser klasse.
            protocols = readyDataset.getProtocols()
            
            # afdruk van protocols lijst.
            print("\n")
            for p, count in protocols.items():
                print(f"{p}: {count} keer worden gebruikt.")
            print("\n")

        # oproep getType methode uit pcktAnalyser klasse.
        tcpType = readyDataset.getType()

        # oproep analyzeHandshakes methode uit pcktAnalyser klasse.
        handshakes = readyDataset.analyzeHandshakes(tcpType)

        # oproep findCompleteHandshakes methode uit pcktAnalyser klasse.
        completeHandshakes = readyDataset.findCompleteHandshakes(tcpType)

        # oproep findInCompleteHandshakes methode uit pcktAnalyser klasse.
        inCompleteHandshakes = readyDataset.findInCompleteHandshakes(tcpType)

        # Als argument -t/ --tcpType wordt uitgevoerd.
        if args.tcpType:

            # afdruk van relevante analysebeschrijvingen.
            print()
            print("Het geïmporteerde dataset wordt geanalyseerd m.b.t. TCP-3 way handshake pakketjes: ")

            # afdruk de lengte van de tcpType dictionary (die het aantal TCP pakketten weergeeft).
            print(f"- Totaal TCP-3 way handshake pakketjes: {len(tcpType)}")

            # Teller van SYN pakketten.
            synId = 0

            # loopt elk item in tcpType dictionary.
            for item in tcpType.values():

                # Voor elk SYN pakket, telt synId één op.
                if item["TYPE"] == "SYN":
                    synId += 1

            # Afdruk van het aantal van SYN pakketjes.
            print(f"- Het aantal pakketten met TYPE 'SYN' (tcp.flag = 0x0002) is: {synId}")

            # Teller van SYN-ACK pakketten.
            synAckId = 0

            # loopt elk item in tcpType dictionary.
            for item in tcpType.values():

                # Voor elk SYN-ACK pakket, telt synAckId één op.
                if item['TYPE'] == 'SYN-ACK':
                    synAckId += 1

            # Afdruk van het aantal van SYN-ACK pakketjes.
            print(f"- Het aantal pakketten met TYPE 'SYN-ACK' (tcp.flag = 0x0012) is: {synAckId}")

            # Teller van ACK pakketten.
            ackId = 0

            # loopt elk item in tcpType dictionary.
            for item in tcpType.values():

                # Voor elk ACK pakket, telt ackId één op.
                if item['TYPE'] == 'ACK':
                    ackId += 1

            # Afdruk van het aantal van ACK pakketjes.
            print(f"- Het aantal pakketten met TYPE 'ACK' (tcp.flag = 0x0010) is: {ackId}")

            # Extra informatie over command-line interface argumenten.
            print("---> Als u de volledige lijst met pakketten wilt zien. Gebruik -t parameter")
            print()

        # Als argument -H/ --handshakes wordt uitgevoerd.
        if args.handshakes:

            # Als de lijst niet leeg is. De lengte van de lijst (aantal van handshakes) wordt afgedrukt.
            if len(handshakes) > 0: 
                print(f"- Er zijn {len(handshakes)} handshakes tot stand gebracht door gebruikers.")

            # Als de lijst leeg is, wordt dit weergegeven via een bericht.
            else:
                print("- Er begonnen geen handshake.")

            print()

            # Als de lijst niet leeg is. De lengte van de lijst (aantal van voltooide handshakes) wordt afgedrukt.
            if len(completeHandshakes) > 0: 
                print(f"- Er bestaan {len(completeHandshakes)} voltooide handshakes." )

            # Als de lijst leeg is, wordt dit weergegeven via een bericht.
            else:
                print("- Er is geen voltooide handshakes.")

            # Extra informatie over command-line interface argumenten.
            print("---> Als u de volledige lijst van voltooide handshakes pakketten wilt zien, gebruik -c parameter.")
            print()

            # Als de lijst niet leeg is. De lengte van de lijst (aantal van onvoltooide handshakes) wordt afgedrukt.
            if len(inCompleteHandshakes) > 0: 
                print(f"- Er bestaan {len(inCompleteHandshakes)} onvoltooide handshakes." )

            # Als de lijst leeg is, wordt dit weergegeven via een bericht.
            else:
                print("- Er is geen onvoltooide handshakes.")

            # Extra informatie over command-line interface argumenten.
            print("---> Als u de volledige lijst van onvoltooide handshakes pakketten wilt zien, gebruik -i parameter")
            print()
    
        # Als argument -c/ --completeHandshakes wordt uitgevoerd.
        if args.completeHandshakes:

            # De lengte van de lijst (aantal van voltooide handshakes) wordt afgedrukt.
            print(f"Lijst van voltooide handshakes (totaal: {len(completeHandshakes)}): ")
            print()

            # Teller van elk voltooide handshakes
            completeHandshakesId = 0

            # loopt elk item in completeHandshakes.
            for sublist in completeHandshakes:

                # voor elk voltooide handshakes telt een op.
                completeHandshakesId += 1
                
                # Afdruk van het resultaat.
                for item in sublist:    
                    print(completeHandshakesId)
                    print(item[0])  
                    print(item[1])
                    print(item[2])
                    print()

        # Als argument -i/ --inCompleteHandshakes wordt uitgevoerd.
        if args.inCompleteHandshakes:

            # De lengte van de lijst (aantal van onvoltooide handshakes) wordt afgedrukt.
            print(f"Lijst van onvoltooide handshakes (totaal: {len(inCompleteHandshakes)}): ")
            print()

            # Teller van elk onvoltooide handshakes
            inCompleteHandshakesId = 0

            # loopt elk item in inCompleteHandshakes.
            for sublist in inCompleteHandshakes:

                # voor elk onvoltooide handshakes telt een op.
                inCompleteHandshakesId += 1

                # Afdruk van het resultaat.
                for item in sublist:
                    print(inCompleteHandshakesId)
                    print(item[0])  
                    print(item[1])
                    print(item[2])
                    print()
