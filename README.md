# System & Middleware Programming (SMP) <2023>
  
This assignment is for the SMP-Python course of Cyber Security Learning Route HBO-ICT, Amsterdam University of Applied Sciences. 

In this assignment, the information from a dataset is analyzed and the necessary information is collected. The dataset consists of captured network traffic. This is traffic to a university web server, and the dataset contains the packets that can be expected from it. Some hosts are connected to the university network, and are therefore on the same subnet as the web server, while other hosts try to visit the website from a different location.

While most hosts communicate properly with the Web server, there are also hosts in this dataset that have less noble goals: some attempt to break into the Web server, flood it with requests, or otherwise take out the Web server to get the air.

It is supposed to use the Python program. A program should be written to detect hosts performing suspicious activities. So that in the next step they can be prevented from accessing (sending the request) the university's server.

## Documentation

For more insight about this product, refer to the documents folder.